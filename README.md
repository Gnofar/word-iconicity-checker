# word-iconicity-checker

This program searches the ASJP dataset (https://asjp.clld.org/) for possible iconic traits according to a number of predefined sets.

The the program is used with typing:
`Python3 check.py `
In a terminal.

For the program to work you need a version of python3 to be installed on your computer, it's necessary to start this program in some sort of terminal for the user-interface to work.
