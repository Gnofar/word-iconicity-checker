# Check if individual word is either hard or soft, not tally vowel consonants
import re
import os
# From Alan Nielsen and Drew Rendell 2011 - Mainly highlights consonants by strident and sonorant qualities.
N_R_set = {"name": "Nielsan and Rendell", "soft":["m","n","L"], "hard": ["k", "p", "t"]}
# From D´Onofrio
V_set = {"name": "Vowel set", "soft":['u','o'], 'hard':['i','e']}
#
search_sets = [N_R_set, V_set]
print("There are:", len(search_sets), "sets to choose from")
for i in search_sets:
    print("choose set?: ", i['name'])
    if input('y/n:') == 'y':
        chosen_set = i
        break
namePattern = re.compile('(\w+){')
def get_words(filedir):
    words = set()
    selectedWords = []
    i = 0
    num_of_words = input("How many words do you want to search for?\n:")
    while i < int(num_of_words):
        words.add(input("Input number (ex write '1' for 'I')\n:"))
        i+=1
    with open(filedir, 'r', encoding="Windows-1252") as f:
        patterns = []
        for word in words:
            patterns.append(word + '\s\w+')
        for line in f:
            line = line.split('\t')
            if namePattern.match(line[0]) != None:
                selectedWords.append(line)
            for pattern in patterns:
                if re.match(pattern,line[0]): selectedWords.append(line)
    return selectedWords, words
def manipulate_words(sortedWords,outputdir):
    with open(outputdir,'r+',encoding="utf-8") as f:
        for word in sortedWords:
            if len(word) > 1 and word[1].find('XXX //') == -1 :f.write(word[0] + ": " + word[1])
            elif len(word) == 1: f.write(word[0])
def searchWords(set,word_numbers,in_file):
    choice = input('(1) Search all language families \n(2) Search specific language families\n(3) Search specific languages\n(4) Search custom search\n:')
    if choice == str(1): #All languages
        with open(in_file, 'r', encoding='utf-8')as f:
            lines = f.readlines()
            total_result = 0
            all_languages = {}
            current_language = {"name" : "", "family" : ""}
            for word_number in word_numbers:
                current_language["word_"+word_number]=""
                current_language["iconicity_soft_"+word_number]= 0
                current_language["iconicity_hard_"+word_number]= 0
                all_languages["iconicity_"+word_number] = 0
            current_language["iconicity_sum"] = 0
            index = 0
            total_iconicity_value_soft = 0
            total_iconicity_value_hard = 0
            for line in lines:
                if index < len(lines) -1:
                    index += 1
                if namePattern.match(line)!= None and namePattern.match(lines[index]) == None:
                    matches = re.match('^(\w+){(.+)\|', line)
                    current_language["name"] = matches.group(1); current_language["family"] = matches.group(2)
                for word_number in word_numbers:
                    #Might need a better solution
                    if line.find(word_number) != -1:
                        word_in_line = list()
                        word_in_line = re.findall('[1-9][0-9]? \w*:(.*)\/\/', line)
                        current_language["word_"+word_number]= word_in_line
                        #print(str(current_language["name"])+ " word is " + str(word_in_line))
                        #checking iconicity
                        iconicity_value_soft = 0
                        iconicity_value_hard = 0
                        if not word_in_line:
                            word_in_line.append('-')
                        for letter in word_in_line[0]:
                            if letter in set["soft"]:
                                iconicity_value_soft += 1
                            elif letter in set["hard"]:
                                iconicity_value_hard += 1
                        if iconicity_value_hard > iconicity_value_soft:
                            current_language["iconicity_"+word_number] = "hard"
                            current_language["iconicity_hard_"+word_number] +=1
                            total_iconicity_value_hard += 1
                        if iconicity_value_soft > iconicity_value_hard:
                            current_language["iconicity"+word_number] = "soft"
                            current_language["iconicity_soft_"+word_number] +=1
                            total_iconicity_value_soft += 1
            print("Soft words are: " + str(total_iconicity_value_soft) + "\nHard words are: " + str(total_iconicity_value_hard))
    elif choice == str(2): #Specific language families
        i = 0
        selected_families = []
        num_of_families = input("How many language families do you want to include?\n:")
        while i < int(num_of_families):
            selected_families.append(input("Input familiy (ex write 'IE.GERMANIC')\n:"))
            i += 1
        with open(in_file, 'r', encoding='utf-8')as f:
            lines = f.readlines()
            total_result = 0
            all_languages = {}
            current_language = {"name" : "", "family" : ""}
            for word_number in word_numbers:
                current_language["word_"+word_number]=""
                current_language["iconicity_soft_"+word_number]= 0
                current_language["iconicity_hard_"+word_number]= 0
                all_languages["iconicity_"+word_number] = 0
            current_language["iconicity_sum"] = 0
            index = 0
            total_iconicity_value_soft = 0
            total_iconicity_value_hard = 0
            match = False
            for line in lines:
                if index < len(lines) -1:
                    index += 1
                if namePattern.match(line)!= None and namePattern.match(lines[index]) == None:
                    matches = re.match('^(\w+){(.+)\|', line)
                    current_language["name"] = matches.group(1); current_language["family"] = matches.group(2)
                    if matches.group(2) in selected_families:
                        match = True
                        print(matches.group(1) + " is " + matches.group(2))
                    elif matches.group(2) not in selected_families:
                        #print(matches.group(1) + " is not a match")
                        match = False
                if match:
                    for word_number in word_numbers:
                        #Might need a better solution
                        if line.find(word_number) != -1:
                            word_in_line = list()
                            word_in_line = re.findall('[1-9][0-9]? \w*:(.*)\/\/', line)
                            current_language["word_"+word_number]= word_in_line
                            print(str(current_language["name"])+ " word is " + str(word_in_line))
                            #checking iconicity
                            iconicity_value_soft = 0
                            iconicity_value_hard = 0
                            if not word_in_line:
                                word_in_line.append('-')
                            for letter in word_in_line[0]:
                                if letter in set["soft"]:
                                    iconicity_value_soft += 1
                                elif letter in set["hard"]:
                                    iconicity_value_hard += 1
                            if iconicity_value_hard > iconicity_value_soft:
                                current_language["iconicity_"+word_number] = "hard"
                                current_language["iconicity_hard_"+word_number] +=1
                                total_iconicity_value_hard += 1
                            if iconicity_value_soft > iconicity_value_hard:
                                current_language["iconicity"+word_number] = "soft"
                                current_language["iconicity_soft_"+word_number] +=1
                                total_iconicity_value_soft += 1
            print("Soft words are: " + str(total_iconicity_value_soft) + "\nHard words are: " + str(total_iconicity_value_hard))
    elif choice == str(3):
        i = 0
        selected_languages = []
        num_of_languages = input("How many languages do you want to search for?\n:")
        while i < int(num_of_languages):
            selected_languages.append(input("Input language (ex write 'SWEDISH')\n:"))
            i += 1
        with open(in_file, 'r', encoding='utf-8')as f:
            lines = f.readlines()
            total_result = 0
            all_languages = {}
            current_language = {"name" : "", "family" : ""}
            for word_number in word_numbers:
                current_language["word_"+word_number]=""
                current_language["iconicity_soft_"+word_number]= 0
                current_language["iconicity_hard_"+word_number]= 0
                all_languages["iconicity_"+word_number] = 0
            current_language["iconicity_sum"] = 0
            index = 0
            total_iconicity_value_soft = 0
            total_iconicity_value_hard = 0
            match = False
            for line in lines:
                if index < len(lines) -1:
                    index += 1
                if namePattern.match(line)!= None and namePattern.match(lines[index]) == None:
                    matches = re.match('^(\w+){(.+)\|', line)
                    current_language["name"] = matches.group(1); current_language["family"] = matches.group(2)
                    if matches.group(1) in selected_languages:
                        match = True
                    elif matches.group(1) not in selected_languages:
                        match = False
                if match:
                    for word_number in word_numbers:
                        #Might need a better solution
                        if line.find(word_number) != -1:
                            word_in_line = list()
                            word_in_line = re.findall('[1-9][0-9]? \w*:(.*)\/\/', line)
                            current_language["word_"+word_number]= word_in_line
                            print(str(current_language["name"])+ " word is " + str(word_in_line))
                            #checking iconicity
                            iconicity_value_soft = 0
                            iconicity_value_hard = 0
                            if not word_in_line:
                                word_in_line.append('-')
                            for letter in word_in_line[0]:
                                if letter in set["soft"]:
                                    iconicity_value_soft += 1
                                elif letter in set["hard"]:
                                    iconicity_value_hard += 1
                            if iconicity_value_hard > iconicity_value_soft:
                                current_language["iconicity_"+word_number] = "hard"
                                current_language["iconicity_hard_"+word_number] +=1
                                total_iconicity_value_hard += 1
                            if iconicity_value_soft > iconicity_value_hard:
                                current_language["iconicity"+word_number] = "soft"
                                current_language["iconicity_soft_"+word_number] +=1
                                total_iconicity_value_soft += 1
            print("Soft words are: " + str(total_iconicity_value_soft) + "\nHard words are: " + str(total_iconicity_value_hard))
    elif choice == str(4):
        i = 0
        previous_groups = []
        with open(in_file, 'r', encoding='utf-8')as f:
            lines = f.readlines()
            total_result = 0
            all_languages = {}
            first_language = True
            statistics_list = []
            cache = dict()
            current_language = {"name" : "", "family" : ""}
            for word_number in word_numbers:
                current_language["word_"+word_number]=""
                current_language["iconicity_soft_"+word_number]= 0
                current_language["iconicity_hard_"+word_number]= 0
                all_languages["iconicity_"+word_number] = 0
            current_language["iconicity_sum"] = 0
            index = 0
            total_iconicity_value_soft = 0
            total_iconicity_value_hard = 0
            match = False
            for line in lines:
                if index < len(lines) -1:
                    index += 1
                if namePattern.match(line)!= None and namePattern.match(lines[index]) == None:
                    matches = re.match('^(\w+){(.+)\|', line)
                    current_language["name"] = matches.group(1); current_language["family"] = matches.group(2)
                    if matches.group(2) in previous_groups:
                        match = False
                    elif matches.group(2) not in previous_groups and matches.group(2) != 'Oth.ARTIFICIAL' and matches.group(2) != 'Oth.FAKE':
                        match = True
                if match and matches.group(2) != 'Oth.ARTIFICIAL' and matches.group(2) != 'Oth.FAKE' and matches.group(2) != 'Oth.SPEECH_REGISTER' and matches.group(2) != 'Oth.UNCLASSIFIED' and matches.group(2) != 'Oth.CREOLES_AND_PIDGINS':
                    for word_number in word_numbers:
                        #Might need a better solution
                        over = 0
                        under = 0
                        result = 0
                        if line.find(word_number) != -1 and line.find('%') == -1:
                            word_in_line = list()
                            word_in_line = re.findall('[1-9][0-9]? \w*:(.*)\/\/', line)
                            current_language["word_"+word_number]= word_in_line
                            #checking iconicity
                            current_language['iconicity'] = 'none'
                            if not word_in_line:
                                word_in_line.append('-')
                            if word_in_line[0].find(','): word_in_line[0] = word_in_line[0].split(',')[0]
                            for letter in word_in_line[0]:
                                #print(letter)
                                if letter in set['soft'] or letter in set['hard']:
                                    under += 1
                                    #print(letter)
                                if letter in set['hard']:
                                    over += 1
                                    #print(letter)
                            if under != 0 :
                                result = (over/under)
                                previous_groups.append(matches.group(2))
                            else: result = 'bah'
                            if first_language:
                                first_language = False
                                cache = current_language['name'] + word_number
                            elif current_language['name'] + word_number != cache:
                                cache = current_language['name'] + word_number
                                #print(current_language)
                                p = word_in_line[0].split(',')[0]
                                prep_string = '%s,%s,%s,%s,%s,%s' % (current_language["name"],current_language["family"], word_number, p.strip(), result, len(p.strip())) #language,family,swadesh_word,word,firmness
                                statistics_list.append(prep_string)
            print("Soft words are: " + str(total_iconicity_value_soft) + "\nHard words are: " + str(total_iconicity_value_hard))
    return statistics_list
def write_statistics(in_statistics,output):
    with open(output, 'w+', encoding='utf-8') as f:
        f.write("Language,Family,Number,Word,Firmness,Length\n")
        for i in in_statistics:
            if i.find('bah')  == -1 :
                f.write(i) #if iconicity == None don't write
                f.write('\n')
result,word_numbers = get_words("listss18.txt")
manipulate_words(result,'output.txt')
statistics = searchWords(chosen_set,word_numbers,"output.txt",)
write_statistics(statistics, 'statistics.csv')
